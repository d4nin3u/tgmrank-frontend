function fillLoginForm(username, password) {
  cy.get('#frmUsernameA').type(username);
  cy.get('#frmPasswordA').type(password);
}

function login() {
  cy.get('[data-action="login"]').click();
}

describe('Login', () => {
  it('Should not login with incorrect credentials', () => {
    cy.visit('/');
    cy.get('[href="/login"]').click();

    fillLoginForm('Hello', 'World');
    login();

    cy.get('.react-toast-notifications__toast--error').should('exist');
    cy.validatePath('/login');
    cy.get('#frmPasswordA').should('be.empty');
  });

  it('Should log in with correct credentials', () => {
    cy.visit('/');
    cy.get('[data-info="login"]').click();

    cy.stubLogin();
    fillLoginForm('LegitUser', 'NoDoubt');
    login();
    cy.stubLoginVerify();

    // Redirected back to home page
    cy.validatePath('/');

    cy.get('[data-action="submit-score"]').click();
    cy.validatePath('/score/submit');

    cy.get('[data-info="login"]').should('not.exist');

    cy.get('[data-info="user-menu"').click();

    cy.stubLogout().as('logout');
    cy.get('[href="/logout"]').should('be.visible');
    cy.get('[href="/logout"]').click();

    cy.wait('@logout').then(() => {
      cy.validatePath('/');
    });
  });

  it('Should redirect to login page when accessing page that require user login', () => {
    cy.visit('/score/submit');
    cy.validatePath('/login');

    cy.visit('/settings');
    cy.validatePath('/login');
  });

  it('Should redirect to 404 page when accessing restricted page', () => {
    cy.visit('/admin');
    cy.validatePath('/not-found');
  });
});
