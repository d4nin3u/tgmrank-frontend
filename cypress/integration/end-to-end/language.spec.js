function selectLanguage(language) {
  const languageMap = new Map();
  languageMap['en'] = 0;
  languageMap['fr'] = 1;
  languageMap['ja'] = 2;

  const languageIndex = languageMap[language.toLowerCase()];

  cy.get('#menu-button--menu--1 > .nav__icon').click();
  cy.get(`#option-${languageIndex}--menu--1`).click();
}

describe('Theme', () => {
  it('Should change language to french and back', () => {
    cy.visit('/');
    cy.get('.homepage__nav h2').should('have.text', 'Browse Leaderboards');
    cy.getCookie('locale').should('have.property', 'value', 'en');

    selectLanguage('fr');
    cy.get('.homepage__nav h2').should('have.text', 'Voir les classements');
    cy.getCookie('locale').should('have.property', 'value', 'fr');

    selectLanguage('en');
    cy.get('.homepage__nav h2').should('have.text', 'Browse Leaderboards');
    cy.getCookie('locale').should('have.property', 'value', 'en');
  });
});
