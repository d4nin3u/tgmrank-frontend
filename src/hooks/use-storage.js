import { useEffect, useState } from 'react';
import { localStorageWrapper } from '../server-helpers';

export default function useLocalStorage(key, initialState) {
  const storage = localStorageWrapper();

  const [item, setValue] = useState(() => {
    const item = storage?.getItem(key);
    if (item == null) {
      storage?.setItem(key, JSON.stringify(initialState));
      return initialState;
    } else {
      // We reloaded our state.
      return { ...JSON.parse(item) };
    }
  });

  useEffect(() => {
    storage?.setItem(key, JSON.stringify(item));
  }, [item]);

  const reset = () => {
    setValue(initialState);
  };

  return [item, setValue, reset];
}
