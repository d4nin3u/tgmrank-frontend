import { useEffect } from 'react';
import { navigate } from '@reach/router';
import { useUser } from '../../contexts/user';
import TgmRank from '../../tgmrank-api';

/**
 * @return {null}
 */
export default function LogoutLink() {
  const { logout, isLoggedIn } = useUser();

  useEffect(() => {
    if (isLoggedIn) {
      TgmRank.logout().then(() => logout());
    }

    navigate('/');
  }, [isLoggedIn]);

  return null;
}
