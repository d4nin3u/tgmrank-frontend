import React from 'react';
import { Helmet } from 'react-helmet-async';
import useSWR from 'swr';
import { LongDateTime } from '../../components/date';
import { toTitleCase } from '../../components/string-utilities';
import Tag from '../../components/tag';

import { notFoundRedirect } from '../404';
import Layout from '../../components/layout';
import Leaderboard from '../../components/leaderboard';
import LeaderboardHeader, {
  LeaderboardNavigation,
  ModeDescription,
} from '../../components/leaderboard-header';
import { ModeTags, useGames } from '../../hooks/use-games';
import TgmRank from '../../tgmrank-api';
import { useLeaderboardFilter } from '../../components/leaderboard-header/filter';
import {
  CarnivalTheme,
  DeathToll,
  DoomsdayCountdown,
  EventStatusTag,
} from '../challenges';

import './styles.css';

const defaultLeaderboard = 'main';

const rankingAlgorithm = {
  Naive: 'NAIVE',
  Percentile: 'PERCENTILE',
};

function getRankingAlgorithm(filter) {
  return filter.experimentalRankings ? rankingAlgorithm.Percentile : null;
}

export default function LeaderboardPage({ game, leaderboard }) {
  leaderboard = leaderboard ?? defaultLeaderboard;

  const { games } = useGames();
  if (games == null) {
    return null;
  }

  const { game: currentGame, mode: currentMode } =
    games.lookup?.(game, leaderboard) ?? {};

  const [rankedModeKey] = currentGame?.lookupRankedMode(leaderboard) ?? [];
  const isModeLeaderboard = currentMode != null;

  if (!currentGame || (!currentMode && rankedModeKey == null)) {
    notFoundRedirect('We could not find the leaderboard you were looking for.');
  }

  return (
    <>
      <Helmet>
        <title>
          {currentMode
            ? `${currentGame.shortName}: ${currentMode.modeName} rankings`
            : currentGame
            ? `${currentGame.gameName} rankings`
            : 'TGM Rank'}
        </title>
      </Helmet>
      <Layout>
        <LeaderboardHeader
          currentGame={currentGame}
          currentMode={currentMode}
        />
        <LeaderboardNavigation game={currentGame} />
        {/* TODO: Put this somewhere better?*/}
        {(currentMode?.tags?.includes(ModeTags.Event) ||
          currentMode?.tags?.includes(ModeTags.Carnival)) && (
          <>
            <div
              style={{
                fontSize: 'var(--ts-8)',
                fontWeight: 600,
                marginTop: '0.25em',
              }}
            >
              {currentMode.modeName}
            </div>
            <div>
              <EventStatusTag mode={currentMode} />
              {currentMode.tags?.map((tag, tagIndex) => (
                <Tag key={tagIndex}>{toTitleCase(tag)}</Tag>
              ))}
              {currentMode?.submissionRange != null && (
                <>
                  <Tag>
                    Start:{' '}
                    {currentMode.submissionRange.start && (
                      <LongDateTime>
                        {currentMode.submissionRange.start}
                      </LongDateTime>
                    )}
                  </Tag>
                  <Tag>
                    End:{' '}
                    {currentMode.submissionRange.end && (
                      <LongDateTime>
                        {currentMode.submissionRange.end}
                      </LongDateTime>
                    )}
                  </Tag>
                </>
              )}
            </div>
          </>
        )}
        {currentMode?.tags?.includes(ModeTags.Event) && (
          <div className="leaderboard-description">
            <ModeDescription modeId={currentMode.modeId} showDefault={false} />
          </div>
        )}
        {currentMode?.tags?.includes(ModeTags.Carnival) && (
          <>
            <CarnivalTheme />
            <DeathToll mode={currentMode} />
          </>
        )}
        {currentMode?.tags?.includes(ModeTags.Doom) && (
          <>
            <DoomsdayCountdown mode={currentMode} />
          </>
        )}
        {isModeLeaderboard
          ? currentMode && <ModeLeaderboard mode={currentMode} />
          : currentGame && (
              <GameLeaderboard
                game={currentGame}
                rankedModeKey={rankedModeKey}
              />
            )}
      </Layout>
    </>
  );
}

export function ModeLeaderboard({ mode }) {
  const { filter } = useLeaderboardFilter();

  const { data: ranking, error } = useSWR(
    TgmRank.getModeRankingUrl(mode.modeId, {
      asOf: filter.asOf,
      locationFilter: filter.locations,
      algorithm: getRankingAlgorithm(filter),
    }).toString(),
  );

  if (error) {
    return <div>{`Error! ${error}`}</div>;
  }

  if (ranking) return <Leaderboard scores={ranking} mode={mode} />;
  else return <div />;
}

export function GameLeaderboard({ game, rankedModeKey }) {
  const { filter } = useLeaderboardFilter();
  const modeIds = game?.rankedModes?.[rankedModeKey];

  const { data: ranking, error } = useSWR(
    TgmRank.getAggregatedRankingUrl(game.gameId, {
      asOf: filter.asOf,
      modeIds,
      locationFilter: filter.locations,
      algorithm: getRankingAlgorithm(filter),
    }).toString(),
  );

  if (error) {
    return <div>{`Error! ${error}`}</div>;
  }

  if (ranking)
    return (
      <Leaderboard scores={ranking} game={game} rankedModeKey={rankedModeKey} />
    );
  else return <div />;
}
