import Tooltip from '@reach/tooltip';
import React, { useMemo } from 'react';
import { Helmet } from 'react-helmet-async';
import { useTable, useSortBy } from 'react-table';
import useSWR from 'swr';
import { PlayerCell } from '../../components/cell';
import Layout from '../../components/layout';
import { useGames } from '../../hooks/use-games';
import TgmRank from '../../tgmrank-api';

function MedalCount({ modeIds }) {
  const { games } = useGames();

  if (modeIds == null) {
    return '';
  }

  if (modeIds?.length === 0) {
    return 0;
  }

  if (games == null) {
    return '';
  }

  const modes = modeIds?.map(modeId => games?.lookupMode(modeId));
  const label = modes?.map(mode => (
    <p key={mode.modeId}>
      {mode?.game?.shortName} - {mode?.modeName}
    </p>
  ));

  return (
    <Tooltip label={label}>
      <span>{modeIds?.length ?? 0}</span>
    </Tooltip>
  );
}

export function StatsPage() {
  const { data: medals, error } = useSWR(TgmRank.getMedalsUrl().toString());

  const data = useMemo(
    () => medals?.map(m => ({ playerName: m.player.playerName, ...m })) ?? [],
    [medals],
  );

  const columns = useMemo(
    () => [
      {
        Header: 'Player',
        accessor: 'playerName',
        Cell: ({ row }) => <PlayerCell player={row.original.player} />,
      },
      { Header: 'Total', accessor: 'total' },
      {
        Header: 'Gold',
        accessor: 'goldCount',
        Cell: ({ row }) => <MedalCount modeIds={row.original.goldModes} />,
      },
      {
        Header: 'Silver',
        accessor: 'silverCount',
        Cell: ({ row }) => <MedalCount modeIds={row.original.silverModes} />,
      },
      {
        Header: 'Bronze',
        accessor: 'bronzeCount',
        Cell: ({ row }) => <MedalCount modeIds={row.original.bronzeModes} />,
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({ columns, data, disableSortRemove: true }, useSortBy);

  return (
    <Layout>
      <Helmet>
        <title>Stats</title>
      </Helmet>
      <div className="medals">
        {medals != null && (
          <table {...getTableProps()}>
            <thead style={{ userSelect: 'none' }}>
              {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map(column => (
                    <th
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column.render('Header')}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? ' 🔽'
                            : ' 🔼'
                          : ''}
                      </span>
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {rows.map(row => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map(cell => {
                      return (
                        <td
                          {...cell.getCellProps()}
                          style={{
                            textAlign: 'center',
                          }}
                        >
                          {cell.render('Cell')}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
      </div>
    </Layout>
  );
}
