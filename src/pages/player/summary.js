import React from 'react';

import useSWR from 'swr';
import LeaderboardRank from '../../components/leaderboard/rank';
import { TimeDisplay } from '../../components/numbers';
import { useGames } from '../../hooks/use-games';
import TgmRank from '../../tgmrank-api';

export default function PlayerSummaryPage({ playerName }) {
  let { games } = useGames();

  const { data: playerData, error } = useSWR(
    playerName ? TgmRank.getPlayerByName(playerName) : null,
  );

  const { data: playerScores } = useSWR(
    playerData?.playerId
      ? TgmRank.getMultiplePlayerPbsUrl([playerData?.playerId])
      : null,
  );

  const { data: overallData } = useSWR(
    TgmRank.getAggregatedRankingUrl(null, {
      modeIds: games?.overall.rankedModes.main,
      playerName,
    }),
  );

  const { data: extendedOverallData } = useSWR(
    TgmRank.getAggregatedRankingUrl(null, {
      modeIds: games?.overall.rankedModes.extended,
      playerName,
    }),
  );

  return (
    games?.map(g => (
      <React.Fragment key={g.gameId}>
        <h1 data-game={g.shortName.toLowerCase()}>
          {g.shortName} (Main:{' '}
          <LeaderboardRank
            rank={
              overallData?.find(
                o =>
                  o.gameId === g.gameId &&
                  o.player.playerId === playerData?.playerId,
              )?.rank
            }
          />
          , Extended:{' '}
          <LeaderboardRank
            rank={
              extendedOverallData?.find(
                o =>
                  o.gameId === g.gameId &&
                  o.player.playerId === playerData?.playerId,
              )?.rank
            }
          />
          )
        </h1>
        <table>
          <tbody>
            {g.modes
              ?.filter(m => g.rankedModes.extended.includes(m.modeId))
              .map(m => {
                const s = playerScores?.find(s => s.modeId === m.modeId);
                const rankedModes = Object.entries(g.rankedModes)
                  .filter(([, modeIds]) => modeIds.includes(s?.modeId))
                  .map(([key]) => key.toLowerCase());
                return (
                  <tr
                    key={m?.modeId}
                    data-entry-empty={s == null}
                    data-entry-ranked-modes={rankedModes.join(' ')}
                    data-entry-grade-line={
                      s?.grade?.line?.toLowerCase() ?? 'none'
                    }
                    className="summary--entry"
                  >
                    <td
                      className="summary--entry-rank"
                      data-entry-rank={s?.rank}
                    >
                      <LeaderboardRank rank={s?.rank} />
                    </td>
                    <td className="summary--entry-mode">{m.modeName}</td>
                    <td
                      className="summary--entry-grade"
                      data-entry-grade={s?.grade?.gradeDisplay}
                    >
                      {s?.grade?.gradeDisplay}
                    </td>
                    <td
                      className="summary--entry-level"
                      data-entry-level={s?.level}
                    >
                      {s?.level}
                    </td>
                    <td
                      className="summary--entry-time"
                      data-entry-time={s?.playtime}
                    >
                      <TimeDisplay time={s?.playtime} />
                    </td>
                    <td
                      className="summary--entry-score"
                      data-entry-score={s?.score}
                    >
                      {s?.score}
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </React.Fragment>
    )) ?? null
  );
}
