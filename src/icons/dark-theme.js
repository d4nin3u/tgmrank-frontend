import React from 'react';

export default function DarkThemeIcon(props) {
  return (
    <svg viewBox="0 0 25 25" {...props}>
      <path d="M9 4L12.5.5 16 4h5v5l3.5 3.5L21 16v5h-5l-3.5 3.5L9 21H4v-5L.5 12.5 4 9V4h5zm.5 13.7a6 6 0 1 0 0-10.4 6 6 0 0 1 0 10.4z" />
    </svg>
  );
}
