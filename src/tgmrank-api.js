import { formPost, get, post } from './api';
import defaultProfilePic from '../images/avatar.svg';

const BASE_URL = process.env.TGMRANK_BASE_URL;

export const LegacyUserRole = {
  Admin: 'admin',
  Verifier: 'verifier',
  User: 'user',
  Banned: 'banned',
};

export const ProofType = {
  Video: 'video',
  Image: 'image',
  Other: 'other',
};

export const ScoreStatus = {
  Unverified: 'unverified',
  Pending: 'pending',
  Legacy: 'legacy',
  Rejected: 'rejected',
  Verified: 'verified',
  Accepted: 'accepted',
};

export function isReviewedStatus(status) {
  return (
    status === ScoreStatus.Verified ||
    status === ScoreStatus.Accepted ||
    status === ScoreStatus.Rejected
  );
}

async function jsonOrError(fetchResponse, defaultErrorString = 'Error') {
  const responseJson = await fetchResponse.json();
  if (!fetchResponse.ok) {
    throw responseJson.error ?? defaultErrorString;
  }
  return responseJson;
}

async function jsonOrErrorModel(fetchResponse) {
  let parsedResponse;
  try {
    parsedResponse = await fetchResponse.json();
  } catch (e) {
    parsedResponse = {
      error: e.message,
    };
  }

  if (!fetchResponse.ok) {
    let errorString = parsedResponse.error;
    if (parsedResponse.dataErrors?.length > 0) {
      const dataErrorString = parsedResponse.dataErrors
        .map(de => de.details)
        .join(', ');
      errorString = `${errorString}: ${dataErrorString}`;
    }

    parsedResponse.errorString = errorString;

    throw { response: fetchResponse, ...parsedResponse };
  }

  return { response: fetchResponse, ...parsedResponse };
}

export default class TgmRank {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  static async login(loginForm) {
    let res = await post(`${BASE_URL}/v1/login`, loginForm);
    return await jsonOrError(res, 'An error occurred while logging in');
  }

  static async logout() {
    let res = await get(`${BASE_URL}/v1/logout`);
    return await jsonOrError(res, 'An error occurred while logging out');
  }

  static async loginVerify() {
    let res = await get(`${BASE_URL}/v1/login/verify`);
    return await res.json();
  }

  static async register(registrationForm) {
    let res = await post(`${BASE_URL}/v1/register`, registrationForm);
    return await jsonOrErrorModel(res, 'An error occurred while registering');
  }

  static async createUser(username) {
    const createUserForm = {
      username: username,
    };

    let res = await post(`${BASE_URL}/v1/register/simple`, createUserForm);
    return await jsonOrErrorModel(res);
  }

  static async claimAccount(username, email) {
    let res = await get(`${BASE_URL}/v1/register/claim/${username}/${email}`);

    return await jsonOrError(
      res,
      'An error occurred while claiming user account',
    );
  }

  static async flushApiCache() {
    let res = await get(`${BASE_URL}/v1/admin/refresh`);
    return await jsonOrErrorModel(res);
  }

  static getAllPlayersUrl() {
    return new URL(`${BASE_URL}/v1/players`);
  }

  static async getPlayers() {
    let res = await get(this.getAllPlayersUrl());
    return await jsonOrError(
      res,
      'An error occurred while fetching player list',
    );
  }

  static async getGames() {
    let res = await get(`${BASE_URL}/v1/game`);
    return await jsonOrError(res, 'An error occurred while fetching games');
  }

  static getGamesUrl() {
    return new URL(`${BASE_URL}/v1/game`);
  }

  getGamesUrl() {
    return new URL(`${this.baseUrl}/v1/game`);
  }

  static async getGrades(modeId) {
    let res = await get(`${BASE_URL}/v1/grade?mode=${modeId}`);
    return await jsonOrError(res, 'An error occurred while fetching grades');
  }

  static async submitScore(scoreForm) {
    let res = await post(`${BASE_URL}/v1/score`, scoreForm);
    return await jsonOrErrorModel(res);
  }

  static async updateScore(scoreForm) {
    let res = await post(`${BASE_URL}/v1/score`, scoreForm, 'PATCH');
    return await jsonOrErrorModel(res);
  }

  static async verifyScore(scoreId, verifyForm) {
    let res = await post(`${BASE_URL}/v1/score/${scoreId}/verify`, verifyForm);
    return await jsonOrErrorModel(res);
  }

  static async sendForgotPasswordRequest(forgotPasswordForm) {
    let res = await post(`${BASE_URL}/v1/forgot_password`, forgotPasswordForm);
    return await jsonOrError(
      res,
      'An error occurred while sending your forgotten password link',
    );
  }

  static async resetForgottenPassword(playerName, resetKey, resetPasswordForm) {
    let res = await post(
      `${BASE_URL}/v1/reset_password/${playerName}/${resetKey}`,
      resetPasswordForm,
    );
    return await jsonOrErrorModel(res);
  }

  static async register_integration(target, external_key) {
    const form = {
      target,
      external_key,
    };

    let res = await post(`${BASE_URL}/v1/integration`, form);

    return await jsonOrErrorModel(res);
  }

  static getPlayerAchievementsUrl({ playerId }) {
    return new URL(`${BASE_URL}/v1/player/${playerId}/achievement`);
  }

  static getAchievementListUrl() {
    return new URL(`${BASE_URL}/v1/achievement`);
  }

  static async getAchievementList() {
    let res = await get(TgmRank.getLocationListUrl());
    return await res.json();
  }

  static async updatePlayerPassword(currentPassword, newPassword) {
    return await TgmRank.updatePlayerInfo({
      currentPassword,
      newPassword,
    });
  }

  static async updatePlayerUsername(newUsername) {
    let res = await post(
      `${BASE_URL}/v1/player/username`,
      {
        username: newUsername,
      },
      'PUT',
    );

    return await jsonOrErrorModel(res);
  }

  static getPlayerUsernameHistoryUrl(playerId) {
    return new URL(`${BASE_URL}/v1/player/${playerId}/username`);
  }

  static getLocationListUrl() {
    return new URL(`${BASE_URL}/v1/location`);
  }

  static async getLocationList() {
    let res = await get(TgmRank.getLocationListUrl());
    return await res.json();
  }

  static async updatePlayerLocation(locationAlpha2Code) {
    let location = locationAlpha2Code;
    if (location.length === 0) {
      location = null;
    }

    let res = await post(
      `${BASE_URL}/v1/player/location`,
      {
        newLocation: location,
      },
      'PUT',
    );

    return await jsonOrErrorModel(res);
  }

  static async updatePlayerRole(playerId, role) {
    let res = await post(
      `${BASE_URL}/v1/player/${playerId}/role/${role.toLowerCase()}`,
    );

    return await jsonOrErrorModel(res);
  }

  static async updatePlayerInfo(form) {
    let res = await post(`${BASE_URL}/v1/player`, form, 'PATCH');
    return await jsonOrErrorModel(res);
  }

  static async uploadAvatar(file) {
    if (file.size > 1024 * 100) {
      throw 'File is too large (>100kb)';
    }

    let formData = new FormData();
    formData.append('avatarFile', file);

    let res = await formPost(`${BASE_URL}/v1/avatar`, formData);

    return await jsonOrErrorModel(res);
  }

  static getAvatarLink(fileId, thumbnail = false) {
    if (fileId == null) {
      return defaultProfilePic;
    }

    if (thumbnail) {
      return `${BASE_URL}/avatar/thumb/${fileId}`;
    }

    return `${BASE_URL}/avatar/${fileId}`;
  }

  static getScoreUrl(scoreId) {
    return new URL(`${BASE_URL}/v1/score/${scoreId}`);
  }

  static async getScore(scoreId) {
    let res = await get(this.getScoreUrl(scoreId));
    return await jsonOrErrorModel(res);
  }

  static getScoreStatusesUrl() {
    return new URL(`${BASE_URL}/v1/scoreStatus`);
  }

  static getPlayerRolesUrl() {
    return new URL(`${BASE_URL}/v1/roles`);
  }

  static getPlayer(playerId) {
    return new URL(`${BASE_URL}/v1/player/${playerId}`);
  }

  static getPlayerByName(playerName) {
    const url = new URL(`${BASE_URL}/v1/player`);
    url.searchParams.append('playerName', playerName);

    return url;
  }

  static getPlayerScores(playerName) {
    const url = new URL(`${BASE_URL}/v1/player/scores`);
    url.searchParams.append('playerName', playerName);

    return url;
  }

  static getPlayerBadgesUrl(playerId) {
    return new URL(`${BASE_URL}/v1/player/${playerId}/badge`);
  }

  static getModeRankingUrl(modeId, query) {
    const url = new URL(`${BASE_URL}/v1/mode/${modeId ?? ''}/ranking`);
    if (query.asOf != null) {
      url.searchParams.append('asOf', query.asOf);
    }
    if (query.playerName != null) {
      url.searchParams.append('playerName', query.playerName);
    }
    const locations = query.locationFilter?.filter(f => f);
    if (locations?.length > 0) {
      url.searchParams.append('locations', locations.join(','));
    }

    if (query.algorithm != null) {
      url.searchParams.append('algorithm', query.algorithm.toLowerCase());
    }

    return url;
  }

  static getAggregatedRankingUrl(gameId, query) {
    const url = new URL(`${BASE_URL}/v1/game/${gameId ?? ''}/ranking`);
    if (query.asOf != null) {
      url.searchParams.append('asOf', query.asOf);
    }
    if (query.playerIds != null) {
      url.searchParams.append('playerIds', query.playerIds.join(','));
    }
    if (query.playerName != null) {
      url.searchParams.append('playerName', query.playerName);
    }
    if (query.modeIds != null) {
      url.searchParams.append('modeIds', query.modeIds.join(','));
    }
    const locations = query.locationFilter?.filter(f => f);
    if (locations?.length > 0) {
      url.searchParams.append('locations', locations.join(','));
    }
    if (query.algorithm != null) {
      url.searchParams.append('algorithm', query.algorithm.toLowerCase());
    }

    return url;
  }

  static getRecentActivityUrl({
    playerName,
    page,
    pageSize,
    modeIds,
    rankThreshold,
    scoreStatuses,
  }) {
    if (BASE_URL == null) {
      return '';
    }
    const url = new URL(`${BASE_URL}/v1/score/activity`);
    if (playerName != null) {
      url.searchParams.append('playerName', playerName);
    }
    if (page != null) {
      url.searchParams.append('page', page);
    }
    if (pageSize != null) {
      url.searchParams.append('pageSize', pageSize);
    }
    if (modeIds != null) {
      url.searchParams.append('modeIds', modeIds.join(','));
      url.searchParams.append('filterModeIds', modeIds.join(','));
    }
    if (rankThreshold != null) {
      url.searchParams.append('rankThreshold', rankThreshold);
    }
    if (scoreStatuses) {
      url.searchParams.append('scoreStatus', scoreStatuses.join(','));
    }

    return url;
  }

  static getRivalsUrl(playerId) {
    return new URL(`${BASE_URL}/v1/player/${playerId}/rivals`);
  }

  static getRivaledByUrl(playerId) {
    return new URL(`${BASE_URL}/v1/player/${playerId}/rivaledBy`);
  }

  static getMultiplePlayerPbsUrl(playerIds) {
    const playerIdList = playerIds.join(',');
    const url = new URL(`${BASE_URL}/v1/mode/ranking`);
    url.searchParams.append('playerIds', playerIdList);

    return url;
  }

  static getModeDetailsUrl(modeId) {
    return new URL(`${BASE_URL}/v1/mode/${modeId}/description`);
  }

  static async addRival(playerId, rivalPlayerId) {
    let res = await post(`${BASE_URL}/v1/player/${playerId}/rival`, {
      rivalPlayerId: rivalPlayerId,
    });
    return await jsonOrErrorModel(res);
  }

  static async removeRival(playerId, rivalPlayerId) {
    let res = await post(
      `${BASE_URL}/v1/player/${playerId}/rival/player/${rivalPlayerId}`,
      null,
      'DELETE',
    );
    return await jsonOrErrorModel(res);
  }

  static twitchStreamsInfoUrl() {
    return new URL(`${BASE_URL}/v1/twitch/streams`);
  }

  static getMedalsUrl() {
    return new URL(`${BASE_URL}/v1/score/statistics/medal`);
  }
}
