/* Ichiro Mihara


                                             `.-:/+/-.``.--..``                                     
                                       .:/+osyyyyhhhhhyhhhhhyyyo-                                   
                                    -/syhhyyyhhhhhhhhhhhhddddhhhys.                                 
                                 -/syyhhhyhyyyyhhhhyyhhhhddddhyyhhh.                                
                               .oyyhhhhhhhhhhhhhhhhhhhhhdhhhddhhhddh-                               
                             `+hddddddddhhhhhhdddhhhhhhhhhyyhdddddddh:                              
                            .sddddmmddddddddddddddhyyssooo+/osyhdddmdd/                             
                            /hhhdmmmmmmmmmmdhhddhhyso+/+o/:://+oyyhmmdd/                            
                            syyyhmmmNNNmmmdyyhhyssoo//:+:::::://+osydddh-                           
                           `yhhyhhmmNNNmddhyysoo++++/////::::::://++yddms                           
                            ydddhddNNmNmmdhy+/+////::::::::::::::://+hmmd`                          
                            +ddddmmmmNNNmhso///////::::::::::::::::::+dmm-                          
                            :dmmmmmdNNNmyo+/////+oooooo+////////++++/:hNN/                          
                            -mmmdmmmNNmy+///+++oooooossyyso++osyyso+//oNN:                          
                            .hhhyddmmmhs++/+oosoooo++ooosysoosyyysso++/dy                           
                            `/o+shyhhdy+++++oosyyyhddyhdhyy+oyddmdyy+//oo                           
                             `:/yhy/ddy+++//++oso+oossooosy:/ssoosoo/:::.                           
                              -+++o-sho///////////+++++////:://+++++/::.                            
                               :/:/-:///::::///////:///+o/::-::ss++//::`                            
                                -:::::/:::://////////+ss/::/::::sys+//:                             
                                `-/:///:://+o+ooo+++osyysossooo+ohhyo/:                             
                                   :/+////+osssyysssso+shddhhyo+osyyo/.                             
                                   `/+++++ossyyyyssyyssssososso+hhs+/.                              
                                .+++++oooosssssssooyhdhhhhyyss+++so/-         `                     
                              `:hNNNd:+oooosyyyyyo//+ooossssoo+//++:      ``.---                    
                              ossmmho::/+oosyhhhh+//:/++oossoo+/:/-   ``.--://.                     
                             -+ssyyss+//+oosyyyhhy/+/:/+/+++o+//o.  `-:////:.                       
                       ``.-:/+ooysyyyso+ooossyyyyh/////ysssosyyhh/--::/+/.                          
                 ``.-::o++oooossyyysyysooosyyhyhhh+////hmmmdhys/:::/+o+-`                           
          ``.--:/++ooo+sysossooosyhhyyysssshhdddddy::::/ss+/::::::::::::-`                          
      `.:///+oo+ooosssoosssosysoooyhdddddddmmdhddmh::::::::::::::::::://+++/-.`                     
   `.-:/+++osssysssssssoosssssyssosyhdmmmmddmmmdddy:::::::::::://///++:/sssooo++:-`                 
  -://++++oossyyhysssssooosssssssooosyhdmmmmddmmmms:::::::/::::////+///:oysssooooo+/.               
`-://+++ooossssyyhysyssssssssssooooosssyhhhdddddmNs:::::::::::///////////sysssssssooo/-`            
::///++++oosssyyyhhysssssyyoooossooosssssyyyhhhdmmy::::::::::://///:::://yyssssssssssso+/-          
:://+++oooosssssyyyhhssssoosooooosoossssssssyyhhdmm:::::::::://///:://+oossssosyyssyhhyysy.         
:///+ooosssssyyysssyhhssssooooooooooossssssssyyhhdy::::::::////:////osyssssso/oysssyhdmdhdy`        
//+++ooossyyyyyyyhyyyhsssyyoooooooooossssssssyyyyo:::::::::::::/++oshyyssssssssssssydmmddmds.       
/+++oooossssssssyyyhhyyssshoossoooossosyyssyyyys:---::::::::::/+sshddhyyysssssssoooshmddmmddy-      
+oooooossssossssssssyyhysshysssssssssssyyyyyyy+----::::::::::/oyhdddddyyyyysssssooooymdmmddddh-     
++osyyyyyssssssssssssssyyshysssssssssssyyyyhy/----:::::::::/+sdddddddhyyyyssssssssooymmmmdddddh`    
++ooosssyyhhhyyssssssssyhsyhyssssyssyyyyyhyo----::::::::://oyddddddhhyyssssssssssssshmNmmdddddd+    
oo+oooooooooosssyyysssssyhyhysyyyyyyyyyyys:----:::::::://+oymddddhhyyyyssssssssssssshmNNmmdy+/--`   
osssooooooooooooooossssssyhhyyyyyyyyyyhy+----:::::::////+oymmddhhyyyyyyssssssssssssyddNNmy//++/:-`  
+++ossssooo+oooooossssssssyhhhhhyhhhhho:----:::::://////+ymmdhhyyyyysssooo+ososssssyddNdoydmmmmdhs  
+++++++ooosssssssssssssssyhhhhhhhhhho:------::::///////+smmdhhyyyyyyyyssssysssysssyhmmNdNNNmmmmmmm+ 
+++++ooooo++ooosssssssssssyhdddhhho:-----::::://///////smmdhhhhyyyyyyssyssyyyssssyyhmNNNNNNmmmmmmmd`
:++++++oosyyyyyysssssssssyyhddds+------::::://////////smmdhhhhhyyyyyyyyyysyyyssssyyhmMMNNNNNmmmmmms-
/+++osyyssooossssssssssssshys+:-----:::::://////////+sNNdhhhhhyyyyyyyyyyyyyyyyssyyhdNMMNNNNNmmmmms-:
*/
