import React, { useContext, useEffect, useState } from 'react';
import TgmRank from '../tgmrank-api';

const UserLocationsContext = React.createContext();

export function UserLocationsProvider({ children }) {
  const [locations, setLocations] = useState(null);

  useEffect(() => {
    async function fetchLocations() {
      const fetchedLocations = await TgmRank.getLocationList();
      setLocations(fetchedLocations);
    }

    fetchLocations();
  }, []);

  return (
    <UserLocationsContext.Provider value={{ locations }}>
      {children}
    </UserLocationsContext.Provider>
  );
}

export function useUserLocations() {
  return useContext(UserLocationsContext);
}
