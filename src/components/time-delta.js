import React from 'react';
import { Trans } from '@lingui/macro';

function playtimeToMs(playtime) {
  const [hours, minutes, seconds] = playtime.split(':');
  return hours * 3.6e6 + minutes * 60000 + seconds * 1000;
}

function msToSeconds(time) {
  return time / 1000;
}

function timeDelta(previous, current) {
  const delta = playtimeToMs(previous) - playtimeToMs(current);
  return msToSeconds(delta);
}

export default function TimeDelta({ previous, current }) {
  if (previous == null || current == null) {
    return null;
  }

  const delta = timeDelta(previous, current);
  if (delta > 0) {
    return (
      <div>
        <Trans>
          Time improved by <b>{delta} s</b>
        </Trans>
      </div>
    );
  } else {
    return null;
  }
}
