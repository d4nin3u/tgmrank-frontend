import React from 'react';
import { Plural } from '@lingui/macro';

let periods = {
  year: 3.154e10,
  month: 2.628e9,
  week: 6.048e8,
  day: 8.64e7,
  hour: 3.6e6,
  minute: 60000,
  now: -Infinity,
};

const timeAgo = timeStamp => {
  let delta = Date.now() - timeStamp;
  let [period, periodSegment] = Object.entries(periods).find(
    ([, value]) => delta >= value,
  );
  let time = Math.floor(delta / periodSegment);
  return [time, period];
};

export default function TimeAgo({ children }) {
  const timeStamp = new Date(children).getTime();
  const [time, period] = timeAgo(timeStamp);
  const periods = {
    year: <Plural value={time} one="1 year ago" other="# years ago" />,
    month: <Plural value={time} one="1 month ago" other="# months ago" />,
    week: <Plural value={time} one="1 week ago" other="# weeks ago" />,
    day: <Plural value={time} one="1 day ago" other="# days ago" />,
    hour: <Plural value={time} one="an hour ago" other="# hours ago" />,
    minute: <Plural value={time} one="a minute ago" other="# minutes ago" />,
    now: 'just now',
  };
  return (
    <time
      title={new Date(timeStamp).toISOString()}
      dateTime={new Date(timeStamp).toISOString()}
    >
      {periods[period]}
    </time>
  );
}
