import React from 'react';

import './styles.css';
import { leaderboardPath, useGames } from '../../hooks/use-games';
import { Link } from '@reach/router';
import BreadcrumbArrow from '../../icons/breadcrumb-arrow';

export function GameLink({ game, rankedModeKey, children, ...props }) {
  return (
    <Link
      onClick={e => e.stopPropagation()}
      to={leaderboardPath({ game, rankedModeKey })}
      {...props}
    >
      {children ?? game?.shortName}
    </Link>
  );
}

export function ModeLink({ game, mode, children, ...props }) {
  return (
    <Link
      onClick={e => e.stopPropagation()}
      to={leaderboardPath({ game, mode })}
      {...props}
    >
      {children ?? <ModeName {...mode} />}
    </Link>
  );
}

export function GameModeLink({ game, mode, rankedModeKey }) {
  if (game == null || mode == null) {
    return null;
  }

  return (
    <>
      <GameLink game={game} rankedModeKey={rankedModeKey} />
      <BreadcrumbArrow />
      <ModeLink game={game} mode={mode} />
    </>
  );
}

export default function ModeName({
  modeId,
  modeName,
  includeSuffix = true,
  includeBadge = true,
}) {
  const { games } = useGames();

  if (!modeName) return null;
  const rankedSuffix = games?.overall?.rankedModes.main.includes(modeId) && '*';
  const extendedRankSuffix =
    games?.overall?.rankedModes.extended.includes(modeId) && '+';

  const matches = modeName.match(/(.+)\s\(([CW])\)/);

  let badge;
  if (matches) {
    const [, cutModeName, ruleTag] = matches;
    const rule = ruleTag === 'C' ? 'Classic' : 'World';
    modeName = cutModeName;
    badge = <span className={`badge badge--${ruleTag}`}>{rule}</span>;
  }

  return (
    <>
      {modeName}
      <span className="mode-name__details">
        {includeSuffix && (
          <sup>
            {extendedRankSuffix}
            {rankedSuffix}
          </sup>
        )}
        {includeBadge && badge}
      </span>
    </>
  );
}
