import React, { useState, useRef } from 'react';
import { Link, navigate } from '@reach/router';
import { Trans } from '@lingui/macro';
import TgmRank from '../../tgmrank-api';
import { useUser } from '../../contexts/user';
import { useToastManager } from '../../hooks/use-toast-manager';

import './styles.css';

export default function LoginForm() {
  const toastManager = useToastManager();
  const [state, setState] = useState({
    form: {
      username: '',
      password: '',
      remember: false,
    },
  });
  const [isProcessing, setProcessing] = useState(false);
  const { login } = useUser();
  const password = useRef(null);

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      setProcessing(true);

      const responseJson = await TgmRank.login(state.form);
      login(responseJson);

      navigate('/');
    } catch (error) {
      toastManager.addError(error);
      setState(prevState => ({
        form: {
          username: prevState.form.username,
          password: '',
          remember: prevState.form.remember,
        },
      }));
      setProcessing(false);
      password.current.focus();
    }
  }

  function handleChange({ target }) {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setState(prevState => ({
      ...prevState,
      form: {
        ...prevState.form,
        [target.name]: value,
      },
    }));
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="grid-form">
        <div className="grid-col-6">
          <label htmlFor="frmUsernameA">
            <Trans>Username</Trans>
          </label>
          <br />
          <input
            type="text"
            name="username"
            value={state.form.username}
            onChange={handleChange}
            id="frmUsernameA"
            placeholder="Username"
            disabled={isProcessing}
            required
          />
        </div>
        <div className="grid-col-6">
          <label htmlFor="frmPasswordA">
            <Trans>Password</Trans>
          </label>
          <br />
          <input
            ref={password}
            type="password"
            name="password"
            value={state.form.password}
            onChange={handleChange}
            id="frmPasswordA"
            placeholder="Password"
            disabled={isProcessing}
            required
          />
        </div>
        <div className="grid-col-6">
          <input
            type="checkbox"
            name="remember"
            value={state.form.remember}
            onChange={handleChange}
            id="frmRemember"
            disabled={isProcessing}
          />
          <label htmlFor="frmRemember">
            <Trans>Remember me</Trans>
          </label>
        </div>
        <div className="grid-col-6">
          <Link className="dim" to="/forgot-password">
            <Trans>Forgot Password?</Trans>
          </Link>
        </div>
        <div className="grid-col-6">
          <button
            data-action="login"
            className="button"
            disabled={isProcessing}
          >
            {isProcessing ? <Trans>Logging in...</Trans> : <Trans>Login</Trans>}
          </button>
        </div>
      </div>
    </form>
  );
}
