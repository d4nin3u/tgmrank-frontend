import React from 'react';
import useSWR from 'swr';
import TgmRank from '../tgmrank-api';
import LeaderboardRank from './leaderboard/rank';

export function UserRanking({ playerName }) {
  const { data: overallRankingData } = useSWR(
    TgmRank.getAggregatedRankingUrl(0, { playerName }).toString(),
  );

  const rankingData = overallRankingData?.[0];
  const rank = rankingData?.rank;
  const points = rankingData?.rankingPoints ?? 0;

  return (
    <div>
      {rank != null && (
        <>
          <LeaderboardRank rank={rank} /> |
        </>
      )}{' '}
      {points} Points
    </div>
  );
}
