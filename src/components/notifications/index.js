import React from 'react';
import {
  Menu,
  MenuList,
  MenuButton,
  MenuItem,
  MenuLink,
} from '@reach/menu-button';
import Bell from './icon';

import './styles.css';

export default function Notifications() {
  return (
    <Menu>
      <MenuButton className="nav-button">
        <div className="notifications">
          <Bell />
          <div className="notifications__count">3</div>
        </div>
      </MenuButton>
      <MenuList className="dropdown">
        <MenuItem onSelect={() => {}}>
          <div>
            Your score of 10:10:92 in TGM Master has been accepted by our
            moderation team.
            <div className="dim">1d</div>
          </div>
        </MenuItem>
        <MenuItem onSelect={() => {}}>
          <div>
            <div>
              Your score of 10:10:92 in TGM Master has been accepted by our
              moderation team.
              <div className="dim">2d</div>
            </div>
          </div>
        </MenuItem>
        <MenuLink className="dropdown__footer" to="/notifications">
          See All
        </MenuLink>
      </MenuList>
    </Menu>
  );
}
