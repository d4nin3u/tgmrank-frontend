import React from 'react';

import './styles.css';

const defaultDisplay = '—';

export const RankingPointsDisplay = ({ points, rankedScoreCount }) => {
  if (points == null) {
    return defaultDisplay;
  }
  return (
    <>
      {points}
      {rankedScoreCount != null && (
        <sup className="rankedScoreCount">{rankedScoreCount}</sup>
      )}
    </>
  );
};

export const TimeDisplay = ({ time }) => {
  return time?.slice(3).slice(0, -1) ?? defaultDisplay;
};

export const GradeDisplay = ({ children, grade }) => {
  if (grade == null) {
    return defaultDisplay;
  }
  const lineClass = grade?.line ? `grade-line--${grade.line}` : '';
  return <span className={lineClass}>{children}</span>;
};

export const LevelDisplay = ({ level }) => {
  return level ?? defaultDisplay;
};

export const ScoreDisplay = ({ score }) => {
  return score ?? defaultDisplay;
};
