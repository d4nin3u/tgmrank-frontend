import React from 'react';
import { Link, useMatch } from '@reach/router';
import objstr from 'obj-str';
import { leaderboardPath, useGames } from '../hooks/use-games';
import { useRef, useEffect } from 'react';

export function LeaderboardNavLink({ game, mode, rankedModeKey, ...props }) {
  const someRef = useRef();

  const gameMatch = useMatch('/:gameId/*');
  const leaderboardMatch = useMatch('/:gameId/:extension');
  const { games } = useGames();

  let isActive;
  if (mode == null) {
    const isGameMatch =
      gameMatch != null && games.lookup?.(gameMatch?.gameId).game != null;
    isActive =
      isGameMatch &&
      (leaderboardMatch?.extension === rankedModeKey ||
        (leaderboardMatch?.extension == null && rankedModeKey === 'main'));
  } else {
    let modeLookupId = leaderboardMatch?.extension;
    isActive =
      games.lookup?.(leaderboardMatch?.gameId, modeLookupId)?.mode?.modeId ===
      mode.modeId;
  }

  useEffect(() => {
    if (isActive) {
      someRef.current.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest',
      });
    }
  }, [isActive]);

  return (
    <Link
      className={objstr({ 'nav-link': true, 'nav-link--active': isActive })}
      to={leaderboardPath({ game, mode, rankedModeKey })}
      ref={someRef}
      {...props}
    />
  );
}

export default function NavLink(props) {
  return (
    <Link
      {...props}
      getProps={({ href, location }) => {
        let encodedHref = encodeURI(href);
        let currentPath = location.pathname.toLowerCase();
        let isCurrent = currentPath === encodedHref.toLowerCase();
        return {
          className: objstr({
            'nav-link': true,
            'nav-link--active': isCurrent,
          }),
        };
      }}
    />
  );
}
