import { navigate } from '@reach/router';
import React, { useState, useEffect } from 'react';
import { Trans } from '@lingui/macro';
import TgmRank, { ScoreStatus } from '../tgmrank-api';
import useSWR from 'swr';
import Wrapper from './cell-wrapper';
import ActivityCell from './cell/activity';
import Spinner from './spinner';

const InfiniteScrollState = {
  INITIAL_LOAD: 0,
  INITIAL: 1,
  SCROLLING: 2,
  END: 3,
};

function RecentActivityPage({
  playerName,
  notableScoresOnly,
  modeIds,
  page = 1,
  setInfiniteScrollState,
  infiniteScrollState,
}) {
  const isPlayerActivity = playerName != null;
  const { data, error } = useSWR(
    TgmRank.getRecentActivityUrl({
      playerName,
      page: page,
      pageSize: 10,
      modeIds,
      ...(notableScoresOnly && {
        rankThreshold: 10,
      }),
      scoreStatuses: [
        ScoreStatus.Legacy,
        ScoreStatus.Pending,
        ScoreStatus.Accepted,
        ScoreStatus.Verified,
      ],
    }).toString(),
  );

  useEffect(() => {
    if (
      infiniteScrollState === InfiniteScrollState.INITIAL_LOAD &&
      page === 1 &&
      data != null
    ) {
      setInfiniteScrollState(InfiniteScrollState.INITIAL);
    }

    if (data?.length === 0) {
      setInfiniteScrollState(InfiniteScrollState.END);
    }
  }, [infiniteScrollState, page, data]);

  if (error) {
    return `Error! ${error}`;
  }

  function onClick(event, recentActivityItem) {
    const link = `/score/${recentActivityItem.score.scoreId}`;
    if (event.ctrlKey || event.metaKey) {
      window.open(link);
    } else {
      navigate(link);
    }
  }

  return (
    data?.map((item, index) => (
      <Wrapper key={index} onClick={event => onClick(event, item)}>
        <ActivityCell
          showProfile={!isPlayerActivity}
          showImprovements={true}
          showDelta={true}
          item={item}
        />
      </Wrapper>
    )) ?? <Spinner />
  );
}

export default function RecentActivity({
  playerName,
  modeIds,
  notableScoresOnly = false,
}) {
  const [pageCount, setPageCount] = useState(1);
  const [infiniteScrollState, setInfiniteScrollState] = useState(
    InfiniteScrollState.INITIAL_LOAD,
  );
  const pages = [];
  for (let i = 0; i < pageCount; i++) {
    pages.push(
      <RecentActivityPage
        key={i}
        page={i + 1}
        playerName={playerName}
        notableScoresOnly={notableScoresOnly}
        modeIds={modeIds}
        setInfiniteScrollState={setInfiniteScrollState}
        infiniteScrollState={infiniteScrollState}
      />,
    );
  }

  function onScroll() {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
      document.documentElement.offsetHeight
    ) {
      return;
    }

    if (infiniteScrollState !== InfiniteScrollState.SCROLLING) {
      return;
    }

    setPageCount(pageCount => pageCount + 1);
  }

  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  }, [infiniteScrollState]);

  return (
    <div className="activity">
      {pages}
      {infiniteScrollState === InfiniteScrollState.INITIAL && (
        <div className="centered">
          <button
            data-info="infinite-scroll-start"
            className="button"
            onClick={() => {
              setInfiniteScrollState(InfiniteScrollState.SCROLLING);
            }}
          >
            <Trans id="infinite-scroll.enable">Infinite Scroll</Trans>
          </button>
        </div>
      )}
      {infiniteScrollState === InfiniteScrollState.END && (
        <div className="centered" data-info="infinite-scroll-end">
          <Trans id="infinite-scroll.end">No More Results</Trans>
        </div>
      )}
    </div>
  );
}
