import React, { useState } from 'react';
import { Trans } from '@lingui/macro';
import { navigate } from '@reach/router';
import TgmRank from '../../tgmrank-api';
import { useToastManager } from '../../hooks/use-toast-manager';

export function ForgotPasswordForm() {
  const toastManager = useToastManager();
  const [form, setForm] = useState({
    form: {
      username: '',
    },
  });

  const [isProcessing, setProcessing] = useState(false);

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      setProcessing(true);
      await TgmRank.sendForgotPasswordRequest(form.form);

      toastManager.addSuccess('Email sent!');

      navigate('/');
    } catch (e) {
      toastManager.addError(e);
    } finally {
      setProcessing(false);
    }
  }

  function handleChange({ target }) {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setForm(prevForm => ({
      ...prevForm,
      form: {
        ...prevForm.form,
        [target.name]: value,
      },
    }));
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="grid-form">
        <div className="grid-col-6">
          <label htmlFor="frmUsernameA">
            <Trans>Username or Email</Trans>
          </label>
          <br />
          <input
            type="username"
            name="username"
            value={form.form.username}
            onChange={handleChange}
            id="frmUsernameA"
            disabled={isProcessing}
            required
          />
        </div>
        <div className="grid-col-6">
          <button className="button" disabled={isProcessing}>
            {isProcessing ? (
              <Trans>Sending Forgot Password Email...</Trans>
            ) : (
              <Trans>Send Forgot Password Email</Trans>
            )}
          </button>
        </div>
      </div>
    </form>
  );
}

export function ResetPasswordForm(props) {
  const toastManager = useToastManager();
  const [form, setForm] = useState({
    playerName: props.playerName,
    resetKey: props.resetKey,
    form: {
      password: '',
      passwordConfirm: '',
    },
  });

  const [isProcessing, setProcessing] = useState(false);

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      if (
        form.form.password &&
        form.form.password !== form.form.passwordConfirm
      ) {
        toastManager.addError('Passwords do not match');
        return;
      }

      setProcessing(true);
      await TgmRank.resetForgottenPassword(
        form.playerName,
        form.resetKey,
        form.form,
      );

      toastManager.addSuccess(
        'Your password has been reset. Redirecting to login page.',
      );

      navigate('/login');
    } catch (e) {
      toastManager.addError(e.errorString ?? e);
    } finally {
      setProcessing(false);
    }
  }

  function handleChange({ target }) {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setForm(prevForm => ({
      ...prevForm,
      form: {
        ...prevForm.form,
        [target.name]: value,
      },
    }));
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="grid-form">
        <div className="grid-col-6">
          <label htmlFor="frmPassword">
            <Trans>Password</Trans>
          </label>
          <br />
          <input
            type="password"
            name="password"
            value={form.form.password}
            onChange={handleChange}
            id="frmPassword"
            placeholder="Password"
            disabled={isProcessing}
            required
          />
        </div>
        <div className="grid-col-6">
          <label htmlFor="frmPasswordConfirm">
            <Trans>Confirm Password</Trans>
          </label>
          <br />
          <input
            type="password"
            name="passwordConfirm"
            value={form.form.passwordConfirm}
            onChange={handleChange}
            id="frmPasswordConfirm"
            placeholder="Password (Again)"
            disabled={isProcessing}
            required
          />
        </div>
        <div className="grid-col-6">
          <button className="button" disabled={isProcessing}>
            {isProcessing ? (
              <Trans>Resetting Password...</Trans>
            ) : (
              <Trans>Reset Password</Trans>
            )}
          </button>
        </div>
      </div>
    </form>
  );
}
