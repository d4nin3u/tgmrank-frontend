import React from 'react';

export default function Wrapper({ onClick, children, ...props }) {
  if (onClick) {
    return (
      <span
        role="link"
        tabIndex="0"
        onKeyDown={event => {
          if (event.key === 'Enter') {
            event.preventDefault();
            onClick(event);
          }
        }}
        onClick={event => {
          const selectedText = window.getSelection().toString();
          if (selectedText?.trim().length > 0) {
            return;
          }
          onClick(event);
        }}
        {...props}
      >
        {children}
      </span>
    );
  } else return <div {...props}>{children}</div>;
}
