import React from 'react';
import useSWR from 'swr';
import { RivalControl } from './rival';
import TgmRank from '../tgmrank-api';

import LeaderboardRank from './leaderboard/rank';
import Link from './link';
import './player-bio.css';
import Flag from './flag';
import { useGames } from '../hooks/use-games';
import GroupDecoration from './decoration';
import { GameRanking } from '../pages/player';
import { matchMap } from './string-utilities';
import Tag from './tag';

import {
  Check as CheckIcon,
  User as UserIcon,
  UserX,
  Award as AwardIcon,
} from 'react-feather';

export function PlayerRanking({ playerName, game, modeIds, title }) {
  const { data: playerRankingData } = useSWR(
    TgmRank.getAggregatedRankingUrl(game?.gameId, {
      modeIds: modeIds ?? game?.rankedModes.main,
      playerName,
    }),
  );

  if (game == null || playerRankingData == null) {
    return null;
  }

  const rankingItem = playerRankingData?.[0];

  return (
    <div className="number">
      <div>
        <LeaderboardRank rank={rankingItem?.rank} />
        {/*{rankingItem?.rankingPoints && `${rankingItem.rankingPoints} Points`}*/}
      </div>
      <div>{title}</div>
    </div>
  );
}

export function PlayerBadges({ playerId }) {
  function Badge({ badge }) {
    badge = matchMap(badge, /(.+) (Participant|Runner-Up)/, matches => {
      return [<UserIcon key={1} size={12} />, ' ', matches[1]];
    });
    badge = matchMap(badge, /(.+) (Champion)/, matches => {
      return [<AwardIcon key={1} size={12} />, ' ', matches[1]];
    });
    badge = matchMap(badge, /(.+) (Anti-participant)/, matches => {
      return [<UserX key={1} size={12} />, ' ', matches[1]];
    });
    const isTransformed = badge.length > 1;
    if (!isTransformed) {
      badge = [<CheckIcon key={1} size={12} />, ' ', badge[0]];
    }
    return badge.map((badgeComponent, i) => (
      <React.Fragment key={i}>{badgeComponent}</React.Fragment>
    ));
  }

  const { data } = useSWR(
    playerId != null ? TgmRank.getPlayerBadgesUrl(playerId) : null,
  );

  return (
    <div>
      {data?.badges.map((badge, i) => (
        <Tag key={i}>
          <Badge badge={badge} />
        </Tag>
      ))}
    </div>
  );
}

export default function PlayerBio({ playerName, playerData }) {
  const { games } = useGames();

  const { data } = useSWR(
    playerData ? null : TgmRank.getPlayerByName(playerName),
  );

  const player = playerData ?? data;

  return (
    <div className="player-bio">
      {player && (
        <>
          <Link to={`/player/${playerName}`}>
            <img
              className="avatar"
              src={TgmRank.getAvatarLink(player?.avatar)}
            />
          </Link>

          <h1>
            <Link to={`/player/${playerName}`}>{player.playerName}</Link>
            <Flag code={player?.location} />
          </h1>
          <GameRanking playerName={playerName} game={games?.overall} />
          <GroupDecoration playerName={playerName} />
          <PlayerBadges playerId={player.playerId} />

          {player?.playerId && (
            <div>
              <RivalControl rivalPlayerId={player.playerId} />
            </div>
          )}
        </>
      )}
    </div>
  );
}
