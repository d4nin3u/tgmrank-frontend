import React, { useState } from 'react';
import { TwitterTweetEmbed } from 'react-twitter-embed';

export function OtherProof({ url }) {
  const [twitterLoaded, setTwitterLoaded] = useState(false);
  if (url.hostname.includes('twitter')) {
    return (
      <div
        className={`score-details__proof-twitter ${
          twitterLoaded ? 'score-details__proof-twitter--ready' : ''
        }`}
      >
        <TwitterTweetEmbed
          tweetId={url.href.split('/').slice(-1)[0]}
          onLoad={() => {
            setTwitterLoaded(true);
          }}
          placeholder={
            <div>
              <p>
                Loading Tweet <a href={url.href}>{url.href}</a>.
              </p>
              <p>If it does not load, your ad-blocker may be blocking it.</p>
            </div>
          }
          options={{
            theme: 'dark',
            dnt: true,
            conversation: 'none',
            align: 'center',
          }}
        />
      </div>
    );
  }

  return (
    <div className="score-details__proof-other">
      <a href={url.href}>{url.href}</a>
    </div>
  );
}
