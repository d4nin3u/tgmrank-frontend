import React, { useState } from 'react';
import ActivityDelta from '../activity-delta';
import { GameModeLink } from '../mode-name';
import PassedPlayers from '../passed-players';
import ProfileGroup from '../profile-group';
import ScoreImprovements from '../score-improvements';
import TimeAgo from '../time-ago';
import TgmRank from '../../tgmrank-api';
import { useGames } from '../../hooks/use-games';
import getStats, { StatsRow } from '../leaderboard/stats';

export default function ActivityCell({
  item,
  showProfile,
  showImprovements,
  showDelta,
}) {
  const { games } = useGames();
  const [showPreviousScore, setShowPreviousScore] = useState(false);

  if (games == null) {
    return null;
  }

  const { game, mode } = games.lookup?.(item.gameId, item.modeId);
  const stats = getStats({ mode });
  return (
    <div className="activity-item">
      <div className="activity-item__top">
        {showProfile && (
          <div>
            <ProfileGroup
              link={`/player/${item.score.player.playerName}`}
              name={item.score.player.playerName}
              subtitle={<TimeAgo>{item.score.createdAt}</TimeAgo>}
              image={TgmRank.getAvatarLink(item.score.player.avatar, true)}
              location={item.score.player.location}
              {...item}
            />
          </div>
        )}
        <div className="player-score">
          <div>
            <GameModeLink game={game} mode={mode} />
          </div>
          <StatsRow
            stats={stats}
            item={
              item.previousScore && showPreviousScore
                ? item.previousScore
                : item.score
            }
            onMouseEnter={() => setShowPreviousScore(true)}
            onMouseLeave={() => setShowPreviousScore(false)}
          />
        </div>
        {showImprovements && (
          <>
            <ScoreImprovements item={item} mode={mode} />
            <PassedPlayers delta={item.delta} game={game} mode={mode} />
          </>
        )}
      </div>
      {showDelta && item.score.rank && (
        <ActivityDelta game={game} mode={mode} delta={item.delta} />
      )}
    </div>
  );
}
