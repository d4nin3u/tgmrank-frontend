import React from 'react';
import objstr from 'obj-str';
import Link from '../link';
import Flag from '../flag';

import defaultProfilePic from '../../../images/avatar.svg';

import './styles.css';

function WithLink({ link, children, ...props }) {
  // TODO: Pass more valid props?
  return link ? (
    <Link className={props.className} to={link}>
      {children}
    </Link>
  ) : (
    <div {...props}>{children}</div>
  );
}

export default function ProfileGroup({
  name,
  title,
  subtitle,
  rtl,
  image,
  location,
  ...props
}) {
  const classNames = objstr({
    'profile-group': true,
    'profile-group--no-name': !(title || name),
    'profile-group--rtl': rtl,
  });

  return (
    <WithLink className={classNames} {...props}>
      <div className="avatar-wrap">
        <img
          className="avatar avatar__thumbnail"
          src={image ?? defaultProfilePic}
        />
      </div>
      {(title || name) && (
        <div className="profile-group__text pack pack--column">
          <div className="profile-group__name">
            {title ?? name} <Flag code={location} />
          </div>
          {subtitle && (
            <div className="profile-group__subtitle">{subtitle}</div>
          )}
        </div>
      )}
    </WithLink>
  );
}
