import React, { useState } from 'react';
import { Select, Trans } from '@lingui/macro';
import { useLingui } from '@lingui/react';

import { leaderboardPath, useGames } from '../../hooks/use-games';
import Link from '../link';
import Dialog from '../dialog';
import ModeName, { GameLink } from '../mode-name';

function LeaderboardName({ rankedModeKey }) {
  const leaderboardName = rankedModeKey;
  return (
    <Select
      value={leaderboardName}
      main="Main"
      extended="Extended"
      secret="Secret"
      big="Big"
      world="World"
      worldExtended="World (Extended)"
    />
  );
}

export function AggregateLeaderboardLabel({
  game,
  rankedModeKey,
  showDescriptionButton = true,
}) {
  const { i18n } = useLingui();
  const [isDialogShown, showDialog] = useState(false);
  const { games } = useGames();

  if (game?.rankedModes[rankedModeKey] == null) {
    // Do something
  }

  const modeIds = game?.rankedModes[rankedModeKey];

  const filteredGames = [];
  for (let game of games) {
    let modes = game.modes?.filter(m => modeIds?.includes(m.modeId));
    if (modes?.length > 0) {
      filteredGames.push({ ...game, modes });
    }
  }

  function AggregateLeaderboardLink({ game, rankedModeKey, children }) {
    return (
      <GameLink
        onClick={() => showDialog(false)}
        game={game}
        rankedModeKey={rankedModeKey}
      >
        {children}
      </GameLink>
    );
  }

  return (
    <>
      <LeaderboardName rankedModeKey={rankedModeKey} />

      {/*<sup>{extended ? '+' : '*'}</sup>*/}
      {showDescriptionButton && (
        <button
          className="badge"
          onClick={event => {
            event.stopPropagation();
            event.preventDefault();
            showDialog(true);
          }}
        >
          ?
        </button>
      )}
      <Dialog
        aria-label="Aggregate leaderboard description"
        isOpen={isDialogShown}
        close={() => showDialog(false)}
      >
        <Trans>
          The{' '}
          <AggregateLeaderboardLink game={game} rankedModeKey={rankedModeKey}>
            {i18n._(game?.shortName ?? '')}{' '}
            <LeaderboardName rankedModeKey={rankedModeKey} /> Leaderboard
          </AggregateLeaderboardLink>{' '}
          is based on your performance in the following modes:
        </Trans>
        {modeIds != null && (
          <ul>
            {filteredGames.map(game => (
              <li key={game.gameId}>
                <AggregateLeaderboardLink game={game}>
                  {game.gameName} ({game.shortName})
                </AggregateLeaderboardLink>
                <ul>
                  {game.modes.map(mode => (
                    <li key={mode.modeId}>
                      <Link
                        onClick={() => showDialog(false)}
                        to={leaderboardPath({ game, mode })}
                      >
                        <ModeName {...mode} />
                      </Link>
                    </li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
        )}
      </Dialog>
    </>
  );
}
